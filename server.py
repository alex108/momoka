from momoka import make_flask

if __name__ == "__main__":
    app = make_flask()
    app.run()
