import sys

from momoka import make_celery

if __name__ == "__main__":
    celery = make_celery()
    celery.worker_main(argv=sys.argv + ["--loglevel=INFO"])
