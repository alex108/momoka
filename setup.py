import io
import re

from setuptools import setup, find_packages

with io.open("README.md", "rt", encoding="utf8") as f:
    readme = f.read()

with io.open("src/momoka/__init__.py", "rt", encoding="utf8") as f:
    version = re.search(r"__version__ = \"(.*?)\"", f.read()).group(1)

setup_requires = [
    "pytest-runner>=4.2",
]

install_requires = [
    "flask>=1.0",
    "werkzeug>=0.14",
    "celery>=4.2",
    "pyyaml>=3.13",
    "redis>=2.10",
    "flask-and-redis>=0.7",
    "coloredlogs>=10.0",
]

tests_require = [
    "pytest>=3.7",
    "coverage>=4.5",
    "pytest-cov>=2.5",
]

setup(
    name="momoka",
    author="Alex A",
    author_email="alex@meido.ninja",
    description="Simple build and deployment bot.",
    long_description=readme,
    version=version,
    classifiers=(
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Web Environment",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.6",
        "Intended Audience :: Developers",
        "Intended Audience :: Information Technology",
        "Topic :: Internet :: WWW/HTTP :: Site Management",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
    ),
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    include_package_data=True,
    zip_safe=False,
    python_requires=">=3.6",
    setup_requires=setup_requires,
    install_requires=install_requires,
    tests_require=tests_require,
)
