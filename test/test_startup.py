import os
import pytest
from unittest.mock import MagicMock, patch, mock_open

import momoka

sample_config = """
ENV: production
SECRET_KEY: my-secret-key
"""

broken_config = """
ENV: production
SECRET_KEY: my-secret-key
:
"""


@patch.dict(os.environ, {})
@patch("os.path.isfile", new=MagicMock(return_value=False))
def test_config_not_set_nor_found():
    with pytest.raises(RuntimeError):
        momoka.load_config(None)


@patch.dict(os.environ, {})
@patch("os.path.isfile", new=MagicMock(return_value=True))
@patch("momoka.open", new=mock_open(read_data=sample_config))
def test_config_not_set_but_found():
    filename, config = momoka.load_config(None)
    assert config == {
        "ENV": "production",
        "SECRET_KEY": "my-secret-key",
    }


@patch.dict(os.environ, {"MOMOKA_CONFIG": "invalid.yaml"})
@patch("os.path.isfile", new=MagicMock(return_value=False))
def test_config_set_but_not_found():
    with pytest.raises(RuntimeError):
        momoka.load_config(None)


@patch.dict(os.environ, {"MOMOKA_CONFIG": "valid.yaml"})
@patch("os.path.isfile", new=MagicMock(return_value=True))
@patch("momoka.open", new=mock_open(read_data=sample_config))
def test_config_set_and_found():
    filename, config = momoka.load_config(None)
    assert config == {
        "ENV": "production",
        "SECRET_KEY": "my-secret-key",
    }


@patch.dict(os.environ, {"MOMOKA_CONFIG": "valid.yaml"})
@patch("os.path.isfile", new=MagicMock(return_value=True))
@patch("momoka.open", new=mock_open(read_data=broken_config))
def test_invalid_config():
    with pytest.raises(RuntimeError):
        momoka.load_config(None)
