import pytest

from momoka.config.parser import inject_globals, inject_locals

TEST_STRING = "str with {{ dynamic_value }}"
TEST_LOCAL_STRING = "str with {{ dynamic_value }} and {{ dynamic_local }}"
TEST_UNFORMATTED = "str with curly braces {1, 2, 3}"

globs = {
    "dynamic_value": "value",
}


def test_inject_globals():
    assert inject_globals(TEST_STRING, globs) == "str with value"


def test_inject_globals_ignores_none():
    assert inject_globals(None, globs) is None


def test_inject_globals_without_globals():
    assert inject_globals(TEST_STRING, None) == TEST_STRING


def test_inject_locals():
    globbed = inject_globals(TEST_LOCAL_STRING, globs)
    locs = {
        "dynamic_local": "local",
    }
    assert inject_locals(globbed, locs) == "str with value and local"


def test_inject_locals_missing():
    globbed = inject_globals(TEST_LOCAL_STRING, globs)
    with pytest.raises(KeyError):
        inject_locals(globbed, {})
