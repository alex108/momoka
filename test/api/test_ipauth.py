import sys
from unittest.mock import MagicMock
from ipaddress import ip_address

from flask import Flask

from momoka.api.ipauth import load_whitelist, is_whitelisted


def test_load_whitelist_using_app_context():
    # Access module privates but what can we do
    sys.modules["momoka.api.ipauth"].network_whitelist = None
    sys.modules["momoka.api.ipauth"].address_whitelist = None
    app = Flask("test")
    app.config.update({
        "HOOK_NETWORK_WHITELIST": [],
        "HOOK_ADDRESS_WHITELIST": ['127.0.0.1'],
    })
    with app.app_context():
        assert is_whitelisted(ip_address("127.0.0.1"))


def test_whitelist_handles_addresses():
    app = MagicMock()
    app.config = {
        "HOOK_NETWORK_WHITELIST": [],
        "HOOK_ADDRESS_WHITELIST": ['127.0.0.1'],
    }
    load_whitelist(app)
    assert is_whitelisted(ip_address("127.0.0.1"))
    assert not is_whitelisted(ip_address("192.168.0.1"))


def test_whitelist_handles_networks():
    app = MagicMock()
    app.config = {
        "HOOK_NETWORK_WHITELIST": ['127.0.0.0/8'],
        "HOOK_ADDRESS_WHITELIST": [],
    }
    load_whitelist(app)
    assert is_whitelisted(ip_address("127.0.0.1"))
    assert is_whitelisted(ip_address("127.1.1.1"))
    assert not is_whitelisted(ip_address("192.168.0.1"))
