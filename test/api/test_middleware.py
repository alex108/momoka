from flask import Flask, jsonify

from momoka.api.ipauth import load_whitelist
from momoka.api.middleware import allow_whitelisted


def test_allow_whitelisted():
    app = Flask("test")
    app.config.update({
        "HOOK_NETWORK_WHITELIST": [],
        "HOOK_ADDRESS_WHITELIST": ['127.0.0.1'],
    })
    load_whitelist(app)

    def mock_route():
        return jsonify({"status": "ok"})

    app.route("/")(allow_whitelisted(mock_route))
    client = app.test_client()
    forbidden = client.get("/", environ_base={'REMOTE_ADDR': '192.168.0.1'})
    assert forbidden.status_code == 403
    assert forbidden.get_json() == {"status": "forbidden"}
    allowed = client.get("/", environ_base={'REMOTE_ADDR': '127.0.0.1'})
    assert allowed.status_code == 200
    assert allowed.get_json() == {"status": "ok"}
