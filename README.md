# Momoka

Momoka is a simple build and deployment server built on
[Flask](http://flask.pocoo.org/), [Celery](http://www.celeryproject.org/) and
[Redis](https://redis.io/). The intention of this project is providing a simple
configurable build and deployment bot for developers with opinionated defaults.
The scope of this project is not the full gamut of functionality, ease of use
and customizability that can be achieved  with [Jenkins](https://jenkins.io/)
together with [Ansible](https://www.ansible.com/) or
[Chef](https://www.chef.io/), just a group of sane defaults that can be extended
programatically.

## Requirements
Momoka requires a running RabbitMQ server, as well as at least one Redis server
instance.

## Installing
Momoka is packaged with setuptools so you only need to install the momoka module
in your python environment of choice.

## Configuring
An example config file is included in the repository as `config.default.yaml`.
Task steps are simple command executions. Do note that if any command requires
elevation, you will have to put the user running the application in sudoers.
Any elevated command configured should be added to sudoers as NOPASSWD, taking
care to not allow any unnecessary commands.

### Web server
As the web server component is built on Flask, configuration keys for the web
server are the builtin configuration values used by it. An additional
`REVERSE_PROXY` key is used in this application to determine whether the web
server is running behind a reverse proxy like Apache with mod_wsgi or nginx.
Running the web server behind a reverse proxy on the public internet is
heavily recommended.

Usually the only keys that need to be set are `SECRET_KEY` and `REVERSE_PROXY`,
as `ENV` defaults to `production` and `DEBUG` defaults to False in `production`.
The complete list of Flask configuration keys can be found
[here](http://flask.pocoo.org/docs/1.0/config/#builtin-configuration-values).

### Task runner
The task runner component is built on Celery, which has a set of built in
configuration keys too. All Celery configuration keys are scoped under the
`CELERY` key, so they should not be set at the root scope as they will be
ignored. The `broker_url` and `result_backend` keys should be set for correct
operation, where `broker_url` should be point to your RabbitMQ instance. The
complete list of Celery configuration keys can be found
[here](http://docs.celeryproject.org/en/latest/userguide/configuration.html#configuration-directives).

## Running
I suggest running Momoka under a process manager like the
[uWSGI Emperor](https://uwsgi-docs.readthedocs.io/en/latest/Emperor.html) or
[Supervisor](http://supervisord.org/). Two launcher scripts: `server.py` and
`tasks.py` are provided but they should only be used for debugging. A production
environment should instead launched by running the WSGI file `wsgi.py` through
[uWSGI](https://uwsgi-docs.readthedocs.io/en/latest/) or
[Gunicorn](http://gunicorn.org/).

As currently the inner API with Redis is not stable, I suggest running FLUSHDB
on the application Redis each time you update this module.

## TODO
- Unit test this monster
- DOCUMENTATION
- Accept parallel tasks in a workflow
- Windows compatibility check
- Persist data permanently (SQL)
- Allow modifying projects at runtime (would probably need them stored in a DB)

## Version history
### 0.2.0
- Reworked inner API with Redis. FLUSHDB is necessary after upgrading.
- Celery config format migrated. Now using the new lower-case settings.
- Unit testing is now integrated into `setup.py`.

### 0.1.0
- Initial public release
