import os
from logging import getLogger
from logging.config import dictConfig
from typing import Tuple

import yaml
from werkzeug.contrib.fixers import ProxyFix

from momoka.ext.flask import Flask
from momoka.ext.celery import Celery
from momoka.logging import default_logging
from momoka.config import load_projects
from momoka.shared import redis
from momoka.runner.celery import init_celery
from momoka.api import api

__all__ = ["application_factory", "make_flask", "make_celery"]

__version__ = "0.2.0"

DEFAULT_CONFIG = "config.yaml"
MOMOKA_CONFIG = "MOMOKA_CONFIG"


# logger = getLogger(__name__)


def load_config(config_filename: str = None) -> Tuple[str, dict]:
    """
    Attempts to load a given config filename. If no filename was given will try
    using the MOMOKA_CONFIG environment variable and then the default config
    filename. Will raise an error if no option was found.

    :param config_filename: Config filename to load.
    :return: Config filename used and parsed config.
    """
    # Config filename was not set.
    if config_filename is None:
        # Try to get environment variable config filename.
        config_filename = os.environ.get(MOMOKA_CONFIG)

    # Environment variable config filename was not set.
    if config_filename is None:
        # Set the config filename to the default only if the file exists.
        if os.path.isfile(DEFAULT_CONFIG):
            config_filename = DEFAULT_CONFIG

    # We ran out of options
    if config_filename is None:
        raise RuntimeError(
            "No config.yaml was found and the environment variable "
            "MOMOKA_CONFIG is not set. Please include a valid config.yaml file "
            "in the current working directory or set MOMOKA_CONFIG to a valid "
            "configuration file."
        )

    # We were giving a config but it was not found
    if not os.path.isfile(config_filename):
        raise RuntimeError(
            "A configuration file was provided but it was not found. Please "
            "make sure the MOMOKA_CONFIG environment variable points to a "
            "valid configuration file."
        )

    with open(config_filename) as f:
        try:
            return config_filename, yaml.load(f)
        except yaml.YAMLError as exc:
            msg = f"Error parsing config file {config_filename}."
            if hasattr(exc, 'problem_mark'):
                mark = exc.problem_mark
                msg += " Error position: "
                msg += f"line {mark.line+1}, column {mark.column+1}."
            raise RuntimeError(msg) from exc


def setup_logging(config_dict: dict):
    logging_config = config_dict.get("LOGGING")
    if logging_config:
        dictConfig(logging_config)
    else:
        dictConfig(default_logging)
        getLogger(__name__).warn(
            "No logging config was provided. Will fall back to defaults."
        )


def application_factory(config_filename: str = None) -> Tuple[Flask, Celery]:
    """
    Application factory function. Both the Flask application and the Celery
    applications will be used in their respective workers, so both are returned
    for individual factory functions to export.
    Based on http://flask.pocoo.org/docs/1.0/patterns/appfactories/.

    :param config_filename: Config file to load.
    :return: Initialized Flask and Celery applications.
    """
    config_filename, config = load_config(config_filename)

    # Setup logging.
    setup_logging(config)

    # Log after setting up logging for obvious reasons.
    getLogger(__name__).info(f"Loaded config file {config_filename}")

    app = Flask("momoka")
    app.config.from_yaml(config_filename)

    # Add ProxyFix middleware if needed.
    if app.config.get("REVERSE_PROXY", False):
        app.wsgi_app = ProxyFix(app.wsgi_app)
        getLogger(__name__).info(f"Accepting reverse proxy headers.")

    # Initialize Redis wrapper.
    redis.init_app(app)

    # Initialize Celery application.
    celery = init_celery(app, ["momoka.runner.tasks"])

    # Load project config.
    projects = load_projects(app.config["PROJECTS"])
    getLogger(__name__).info(f"Loaded projects: {', '.join(projects)}.")

    # Register blueprints
    app.register_blueprint(api, url_prefix="/api")
    return app, celery


def make_flask(config_filename: str = None) -> Flask:
    """
    Flask application factory function. Returns only the Flask application for
    easier integration.

    :param config_filename: Config file to load.
    :return: Initialized Flask application.
    """
    app, celery = application_factory(config_filename)
    return app


def make_celery(config_filename: str = None) -> Celery:
    """
    Celery application factory function. Returns only the Celery application for
    easier integration.

    :param config_filename: Config file to load.
    :return: Initialized Celery application.
    """
    app, celery = application_factory(config_filename)
    return celery
