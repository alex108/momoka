import logging
import yaml

__all__ = ["default_logging"]


class LevelFilter:
    __slots__ = ("_level",)

    def __init__(self, level: str):
        self._level = getattr(logging, level)

    def filter(self, record):
        return record.levelno <= self._level


default_logging = yaml.load("""
    version: 1
    formatters:
      colored:
        (): coloredlogs.ColoredFormatter
        format: "[{asctime}] [{levelname}] {message}"
        datefmt: '%Y-%m-%d %H:%M:%S'
        style: "{"
    filters:
      info_only:
        (): momoka.logging.LevelFilter
        level: INFO
    handlers:
      console_out:
        class: logging.StreamHandler
        formatter: colored
        level: INFO
        filters: [info_only]
        stream: ext://sys.stdout
      console_err:
        class: logging.StreamHandler
        formatter: colored
        level: WARNING
        stream: ext://sys.stderr
    root:
      level: INFO
      handlers: [console_out, console_err]
""")
