from flask import Blueprint, jsonify, abort

from momoka.config import get_project
from momoka.shared import lock, status
from momoka.runner.workflow import run_workflow_async
from momoka.api.middleware import allow_whitelisted

__all__ = ["api"]

api = Blueprint("api", __name__)


@api.route("/<project>/<workflow>/hook", methods=["GET"])
@allow_whitelisted
def project_workflow_hook(project, workflow):
    # Get project (if exists)
    project = get_project(project)
    if not project:
        abort(404)
    # Get workflow (if exists)
    workflow = project.workflows.get(workflow, None)
    if not workflow:
        abort(404)
    # Check if we are currently deploying.
    if lock.is_locked(project.name):
        return jsonify({"status": "busy"}), 400
    try:
        run_workflow_async(project, workflow)
        return jsonify({"status": "ok"}), 200
    except lock.ProjectLocked:
        # Project lock was acquired by other thread after the check.
        return jsonify({"status": "busy"}), 400


@api.route("/<project>/<workflow>/status", methods=["GET"])
def project_workflow_status(project, workflow):
    # Get project (if exists)
    project = get_project(project)
    if not project:
        abort(404)
    # Get workflow (if exists)
    workflow = project.workflows.get(workflow, None)
    if not workflow:
        abort(404)
    result = status.get_status(project.name, workflow.name)
    if not result:
        return jsonify({"status": "not_found"}), 404
    return jsonify(result), 200
