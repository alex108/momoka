from functools import wraps
from ipaddress import ip_address

from flask import request, jsonify

from momoka.api.ipauth import is_whitelisted


def allow_whitelisted(route):
    """
    Decorator middleware applied to a route function. Allows only addresses
    specified in the hook whitelists.

    :param route: Route function.
    :return: Decorated route function.
    """
    @wraps(route)
    def wrapper(*args, **kwargs):
        # Remote IP address for whitelisting
        addr = ip_address(request.remote_addr)
        if is_whitelisted(addr):
            return route(*args, **kwargs)
        else:
            return jsonify({"status": "forbidden"}), 403

    return wrapper
