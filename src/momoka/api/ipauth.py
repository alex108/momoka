from ipaddress import ip_address, ip_network
from typing import List

from flask import Flask, current_app

network_whitelist: List[str] = None
address_whitelist: List[str] = None


def load_whitelist(app: Flask):
    global network_whitelist, address_whitelist
    network_whitelist = [
        ip_network(net) for net in app.config.get("HOOK_NETWORK_WHITELIST", [])
    ]
    address_whitelist = [
        ip_address(addr) for addr in
        app.config.get("HOOK_ADDRESS_WHITELIST", [])
    ]


def is_whitelisted(ip_addr: ip_address):
    if network_whitelist is None or address_whitelist is None:
        load_whitelist(current_app)
    for network in network_whitelist:
        if ip_addr in network:
            return True
    return ip_addr in address_whitelist
