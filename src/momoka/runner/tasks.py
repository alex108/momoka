import os
import subprocess
import shlex
from subprocess import SubprocessError, TimeoutExpired, CalledProcessError

from celery.utils.log import get_task_logger
from redis import RedisError

from momoka.runner.celery import celery
from momoka.config import get_project
from momoka.config.parser import inject_locals
from momoka.config.task import (
    MomokaTaskRunStep,
    MomokaTaskAssignStep,
)
from momoka.shared import lock, status

__all__ = ["fetch", "checkout", "exec_workflow_task", "finish_workflow"]

logger = get_task_logger(__name__)

os_path = os.getenv("PATH")


@celery.task
def exec_workflow_task(project_name: str, workflow_name: str, task_name: str):
    logger.info(f"Executing task {task_name} in workflow {workflow_name} from "
                f"project {project_name}.")
    tasks = get_project(project_name).tasks
    task = tasks[task_name]
    local = {}
    try:
        for step in task.steps:
            if isinstance(step, MomokaTaskRunStep):
                run_kw = step.run_kwargs(local=local)
                subprocess.run(shlex.split(inject_locals(step.command, local)),
                               check=True, **run_kw)
            elif isinstance(step, MomokaTaskAssignStep):
                local[step.variable] = step.executor(**step.kwargs)
        logger.info(f"Task {task_name} in workflow {workflow_name} from "
                    f"project {project_name} finished successfully.")
        status.update_task(project_name, workflow_name, task_name, "SUCCESS")
    except SubprocessError as ex:
        # Do not try to recover, abort
        msg = (f"Task {task_name} in workflow {workflow_name} from "
               f"project {project_name} failed")
        if isinstance(ex, CalledProcessError):
            msg += f" during '{ex.cmd}' (exit code {ex.returncode})."
        elif isinstance(ex, TimeoutExpired):
            msg += f" during '{ex.cmd}' (timed out)."
        else:
            msg += "."
        logger.error(msg)
        status.update_task(project_name, workflow_name, task_name, "FAILED")
        raise


# Other built-in tasks
@celery.task
def fetch(repo_root: str):
    subprocess.run(["git", "fetch", "--all"], cwd=repo_root)


@celery.task
def checkout(repo_root: str, branch: str):
    subprocess.run(["git", "checkout", branch], cwd=repo_root)
    subprocess.run(["git", "reset", "--hard", f"origin/{branch}"],
                   cwd=repo_root)


@celery.task(autoretry_for=(RedisError,),
             retry_kwargs={'max_retries': 3})
def finish_workflow(project_name: str, workflow_name: str, token: str):
    """
    Task that finishes a workflow execution. Should be added to the signature
    chain both on successful finish and on error.

    :param project_name: Project name.
    :param workflow_name: Workflow name.
    :param token: Locking token acquired for execution.
    :return:
    """
    # Unlock the project
    lock.unlock_project(project_name, token)
    # Update shared
    status.end_workflow(project_name, workflow_name)
