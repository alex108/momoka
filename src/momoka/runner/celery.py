from typing import List

from celery import Celery, signals
from flask import Flask

__all__ = ["celery", "init_celery"]

celery = Celery("momoka", autofinalize=False)


# Stop celery from hijacking the logger.
@signals.setup_logging.connect
def config_loggers(*args, **kwargs):
    pass


def config_sanity_check(app: Flask) -> None:
    """
    Run a sanity check on the config provided with the application. If errors
    are found abort the process by raising a RuntimeError.

    :param app: Flask application which will be used for configuration.
    :return:
    """
    if app.config.get("CELERY", None) is None:
        raise RuntimeError(
            "No celery configuration was found in the config file provided. "
            "Please add a CELERY key with at least a broker_url and "
            "result_backend subkeys."
        )
    if app.config["CELERY"].get("broker_url", None) is None:
        raise RuntimeError(
            "No celery broker was configured. Please add a broker_url key to "
            "the CELERY section in the config."
        )
    if app.config["CELERY"].get("result_backend", None) is None:
        raise RuntimeError(
            "No celery broker was configured. Please add a result_backend key "
            "to the CELERY section in the config."
        )


def init_celery(app: Flask, task_modules: List[str]) -> Celery:
    """
    Creates a Celery instance that is configured using a Flask application
    config and that uses the flask application context.

    Based on snippet http://flask.pocoo.org/docs/1.0/patterns/celery/.

    :param app: Flask application which will be used for configuration and as
                Task context.
    :param task_modules: List of task modules to load by default.
    :return: Celery bound application.
    """
    global celery
    config_sanity_check(app)
    # Update include separately to always include the task modules
    celery.conf.include = task_modules + app.config["CELERY"].get("include", [])
    celery.conf.update({
        k: v for k, v in app.config["CELERY"].items()
        if k != "include"
    })

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    setattr(celery, "Task", ContextTask)
    celery.finalize()
    return celery
