from typing import Tuple

from celery import group, chain
from celery.canvas import Signature
from celery.result import AsyncResult

from momoka.config.project import MomokaProject
from momoka.config.repository import Repository
from momoka.config.task import MomokaTask
from momoka.config.workflow import MomokaWorkflow
from momoka.shared import status, lock
from momoka.runner.tasks import (
    fetch as fetch_task,
    checkout as checkout_task,
    exec_workflow_task,
    finish_workflow,
)

__all__ = ["prepare_workflow", "workflow_error_handler", "run_workflow_async"]


def fetch(repository: Repository) -> Signature:
    return fetch_task.si(repository.path)


def checkout(repository: Repository) -> Signature:
    return checkout_task.si(repository.path, repository.branch)


def pull(repository: Repository) -> Signature:
    return fetch(repository) | checkout(repository)


def _handle_workflow_task(project: MomokaProject,
                          workflow: MomokaWorkflow,
                          task: MomokaTask) -> Signature:
    if task.name == "pull":
        if len(project.repositories) == 0:
            repo, = project.repositories
            return pull(repo)
        else:
            return group(pull(repo) for repo in project.repositories)
    else:
        status.add_task(project.name, workflow.name, task.name)
        return exec_workflow_task.si(project.name, workflow.name, task.name)


def prepare_workflow(project: MomokaProject,
                     workflow: MomokaWorkflow) -> Tuple[Signature, str]:
    """
    Creates a Celery signature for a workflow execution. Locks the project given
    in order to avoid conflicts during execution. Make sure to pass
    workflow_error_handler to apply_async so that the project does not end up
    deadlocked.

    :param project: Momoka project instance.
    :param workflow: Momoka project workflow instance.
    :return: Celery task signature and lock token.
    """
    token = lock.lock_project(project.name)
    status.start_workflow(project.name, workflow.name)
    first, remaining = workflow.tasks[0], workflow.tasks[1:]
    # Celery canvas signature
    sig = _handle_workflow_task(project, workflow, first)
    if len(remaining) > 0:
        sig = chain(sig, *[_handle_workflow_task(project, workflow, task)
                           for task in remaining])
    return sig | finish_workflow.si(project.name, workflow.name, token), token


def workflow_error_handler(project: MomokaProject,
                           workflow: MomokaWorkflow,
                           token: str) -> Signature:
    """
    Creates a Celery signature for a workflow error handling. Can be passed on
    apply_async as link_error. Requires the token acquired when preparing the
    workflow.

    :param project: Momoka project instance.
    :param workflow: Momoka project workflow instance.
    :param token: Token acquired when locking.
    :return: Celery task signature
    """
    return finish_workflow.si(project.name, workflow.name, token)


def run_workflow_async(project: MomokaProject,
                       workflow: MomokaWorkflow) -> AsyncResult:
    """
    Prepares and executes a workflow asynchronously. Returns a celery
    AsyncResult.

    :param project: Momoka project instance.
    :param workflow: Momoka project workflow instance.
    :return:
    """
    sign, token = prepare_workflow(project, workflow)
    return sign.apply_async(
        link_error=workflow_error_handler(project, workflow, token)
    )
