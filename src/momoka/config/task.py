"""
Classes defining task behavior. In order to distinguish between Celery tasks
and application tasks all class names are prefixed.
"""
from typing import Union, List

from momoka.config.exc import InvalidProjectConfigurationError
from momoka.config.executors import get_executor
from momoka.config.parser import inject_globals, inject_locals

__all__ = ["MomokaTask", "MomokaTaskStep", "MomokaTaskRunStep",
           "MomokaTaskAssignStep"]


class MomokaTaskStep:
    @staticmethod
    def from_dict(d: Union[dict, str], globs: dict):
        if isinstance(d, str):
            return MomokaTaskRunStep(command=d, globs=globs)
        else:
            if len(d) > 1:
                raise InvalidProjectConfigurationError(
                    "A step cannot have multiple keys."
                )
            if "run" in d:
                sd = d["run"]
                return MomokaTaskRunStep(
                    command=sd["command"],
                    chdir=sd.get("chdir", None),
                    env=sd.get("env", None),
                    globs=globs
                )
            elif "assign" in d:
                sd = d["assign"]
                return MomokaTaskAssignStep(
                    executor=get_executor(sd["executor"]),
                    variable=sd["variable"],
                    kwargs={
                        key: sd[key] for key in sd
                        if key not in ("executor", "variable")
                    },
                    globs=globs
                )


class MomokaTaskRunStep(MomokaTaskStep):
    __slots__ = ("command", "chdir", "env")

    def __init__(self, command: str, globs: dict, chdir: str = None,
                 env: dict = None, ):
        self.command = inject_globals(command, globs=globs)
        self.chdir = inject_globals(chdir, globs=globs)
        if env:
            self.env = {
                key: inject_globals(env[key], globs=globs) for key in env
            }

    def run_kwargs(self, local: dict):
        kw = {}
        if self.chdir:
            kw["cwd"] = inject_locals(self.chdir, local=local)
        if self.chdir:
            kw["env"] = {
                k: inject_locals(self.env[k], local=local) for k in self.env
            }
        return kw


class MomokaTaskAssignStep(MomokaTaskStep):
    __slots__ = ("variable", "executor", "kwargs")

    def __init__(self, variable: str, executor, globs: dict,
                 kwargs: dict = None):
        self.variable = variable
        self.executor = executor
        if kwargs:
            self.kwargs = {
                kwarg:
                    inject_globals(kwargs[kwarg], globs=globs)
                    if isinstance(kwargs[kwarg], str) else kwargs[kwarg]
                for kwarg in kwargs
            }
        else:
            self.kwargs = {}


class MomokaTask:
    def __init__(self, name: str, steps: List[MomokaTaskStep]):
        self.name = name
        self.steps = steps

    @classmethod
    def from_dict(cls, name: str, steps: list, globs: dict):
        if len(steps) == 0:
            raise InvalidProjectConfigurationError(
                "A task must have a step list."
            )
        return cls(
            name=name,
            steps=[
                MomokaTaskStep.from_dict(v, globs=globs) for v in steps
            ]
        )
