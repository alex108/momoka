from typing import Optional, List

from momoka.config.project import MomokaProject

__all__ = ["load_projects", "get_project"]

projects = {}


def load_projects(config: dict) -> List[str]:
    """
    Loads all projects specified in the PROJECTS key of a dict. Returns a list
    of all the projects loaded.

    :param config: Configuration dict.
    :return: List of all projects loaded.
    """
    for k, v in config.items():
        projects[k] = MomokaProject.from_dict(k, v)
    return list(projects.keys())


def get_project(name: str) -> Optional[MomokaProject]:
    return projects.get(name, None)
