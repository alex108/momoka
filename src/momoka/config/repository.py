from momoka.config.parser import inject_globals

__all__ = ["Repository"]


class Repository:
    __slots__ = ("name", "path", "branch")

    def __init__(self, name: str, path: str, branch: str):
        self.name = name
        self.path = path
        self.branch = branch

    @classmethod
    def from_dict(cls, name: str, d: dict, globs: dict):
        return cls(
            name=name,
            path=inject_globals(d["path"], globs=globs),
            branch=d["branch"]
        )
