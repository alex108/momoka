from typing import List, Dict

from momoka.config.exc import InvalidProjectConfigurationError
from momoka.config.task import MomokaTask

__all__ = ["MomokaWorkflow"]


class MomokaWorkflow:
    def __init__(self, name: str, tasks: List[MomokaTask]):
        self.name = name
        self.tasks = tasks

    @classmethod
    def from_dict(cls, name: str, steps: list, tasks: Dict[str, MomokaTask]):
        if len(steps) == 0:
            raise InvalidProjectConfigurationError(
                "A workflow must have a list of steps."
            )
        return cls(
            name=name,
            tasks=[
                tasks[name] for name in steps
                if name not in ("fetch", "checkout", "pull")
            ]
        )
