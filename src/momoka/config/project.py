from typing import Dict

from momoka.config.exc import InvalidProjectConfigurationError
from momoka.config.repository import Repository
from momoka.config.task import MomokaTask
from momoka.config.workflow import MomokaWorkflow

VALID_PROJECT_KEYS = {"globals", "repositories", "tasks", "workflows"}


class MomokaProject:
    __slots__ = ("name", "globs", "repositories", "tasks", "workflows")

    def __init__(self, name: str, globs: dict,
                 repositories: Dict[str, Repository],
                 tasks: Dict[str, MomokaTask],
                 workflows: Dict[str, MomokaWorkflow]):
        self.name = name
        self.globs = globs
        self.repositories = repositories
        self.tasks = tasks
        self.workflows = workflows

    @classmethod
    def from_dict(cls, name: str, d: dict):
        if len(set(d.keys()) - VALID_PROJECT_KEYS) > 0:
            raise InvalidProjectConfigurationError(
                "Unexpected keys found in project."
            )
        name = name
        globs = d.get("globals", {})
        repositories = {
            k: Repository.from_dict(k, sd, globs=globs)
            for k, sd in d.get("repositories", {}).items()
        }
        tasks = {
            k: MomokaTask.from_dict(k, sd, globs=globs)
            for k, sd in d.get("tasks", {}).items()
        }
        workflows = {
            k: MomokaWorkflow.from_dict(k, sd, tasks=tasks)
            for k, sd in d.get("workflows", {}).items()
        }
        return cls(
            name=name,
            globs=globs,
            repositories=repositories,
            tasks=tasks,
            workflows=workflows,
        )
