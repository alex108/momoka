import os
import subprocess
import shlex

__all__ = ["executor", "get_executor"]

executors = {}


def executor(f):
    executors[f.__name__] = f
    return f


def get_executor(name: str):
    return executors[name]


@executor
def check_command_output(command: str):
    return subprocess.check_output(shlex.split(command)).decode("utf-8").strip()


@executor
def get_path():
    return os.getenv("PATH")
