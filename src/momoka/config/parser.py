import re
from typing import Optional

DYNAMIC_VALUE_REGEX = re.compile(r"{{ [^{]+ }}")


def inject_globals(value: Optional[str],
                   globs: Optional[dict]) -> Optional[str]:
    """
    Inject the variables in the globs dict into a configuration string. If a
    variable is not found it is assumed to be a local variable and ignored.
    Variables are formatted as {{ variable_name }}.

    :param value: String to modify. Albeit optional, this is just a fail-safe
                  that NOPs the operation returning None if None was passed.
    :param globs: Globals dictionary.
    :return:
    """
    if not value:
        return None  # NOP
    if not globs:
        globs = {}
    for match in re.finditer(DYNAMIC_VALUE_REGEX, value):
        match_str = match.group(0)
        format_str = match_str.replace("{{ ", "{").replace(" }}", "}")
        try:
            value = value.replace(match_str, format_str.format(**globs))
        except KeyError:
            # We do not have the given variable yet, wait for runtime.
            pass
    return value


def inject_locals(value: Optional[str], local: Optional[dict]) -> Optional[str]:
    """
    Inject the variables in the local dict into a configuration string. If a
    variable is not found KeyError will be raised. Variables are formatted as
    {{ variable_name }}.

    :param value: String to modify. Albeit optional, this is just a fail-safe
                  that NOPs the operation returning None if None was passed.
    :param local: Locals dictionary.
    :return:
    """
    if not value:
        return None  # NOP
    if not local:
        local = {}
    for match in re.finditer(DYNAMIC_VALUE_REGEX, value):
        match_str = match.group(0)
        format_str = match_str.replace("{{ ", "{").replace(" }}", "}")
        value = value.replace(match_str, format_str.format(**local))
    return value
