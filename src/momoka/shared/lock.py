from redis.lock import Lock

from momoka.shared.redis import redis

__all__ = ["is_locked", "lock_project", "ProjectLocked"]


class ProjectLocked(Exception):
    pass


def is_locked(project_name: str):
    return redis.connection.get(f"{project_name}_running")


def lock_project(project_name: str) -> str:
    """
    Acquires a lock over the given project name. Returns the lock identifier
    in order to release the lock later on.

    :param project_name: Project name to lock.
    :return:
    """
    lock = Lock(redis=redis.connection, name=f"{project_name}_running",
                timeout=600, blocking=False)
    acquired = lock.acquire()
    if not acquired:
        raise ProjectLocked(f"Project {project_name} is currently locked.")
    return lock.local.token.decode("ascii")


def unlock_project(project_name: str, token: str):
    """
    Releases a lock over the given project name. Uses the lock token to verify
    the lock is currently owned.

    :param project_name: Project name to unlock.
    :param token: Lock token.
    :return:
    """
    lock = Lock(redis=redis.connection, name=f"{project_name}_running",
                timeout=600, blocking=False)
    lock.local.token = token.encode("ascii")
    lock.release()
