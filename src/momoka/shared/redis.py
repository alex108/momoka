from flask_redis import Redis

__all__ = ["redis"]

redis = Redis()
