"""
Handles the storage of task execution shared in Redis.
"""
import json
from typing import Optional

from momoka.shared.redis import redis

__all__ = ["start_workflow", "end_workflow", "add_task", "update_task",
           "get_status"]


def new_workflow_status(project_name: str, workflow_name: str,
                        state: str) -> dict:
    return {
        "project": project_name,
        "workflow": workflow_name,
        "state": state,
        "tasks": {},
        "order": [],
    }


def new_task_status(task_name: str, state: str) -> dict:
    return {
        "name": task_name,
        "state": state,
    }


def lost_workflow_status(project_name: str, workflow_name: str) -> dict:
    return {
        "project": project_name,
        "workflow": workflow_name,
        "state": "INFORMATION_LOST",
        "tasks": {},
        "order": [],
    }


def status_is_lost(status: dict) -> bool:
    return status["state"] == "INFORMATION_LOST"


def start_workflow(project_name: str, workflow_name: str) -> None:
    """
    Register a workflow in memory as currently working.

    :param project_name: Project name.
    :param workflow_name: Workflow name.
    :return:
    """
    p_status = f"{project_name}_{workflow_name}_status"
    status = new_workflow_status(project_name, workflow_name, "WORKING")
    redis.connection.set(p_status, json.dumps(status))


def end_workflow(project_name: str, workflow_name: str) -> None:
    """
    Register a workflow in memory as finished.

    :param project_name: Project name.
    :param workflow_name: Workflow name.
    :return:
    """
    p_status = f"{project_name}_{workflow_name}_status"
    status = redis.connection.get(p_status)
    if status:
        status = json.loads(status)
        if not status_is_lost(status):
            status["state"] = "FINISHED"
            for task in status["tasks"]:
                if status["tasks"][task]["state"] not in ("SUCCESS", "FAILURE"):
                    status["tasks"][task]["state"] = "ABORTED"
    else:
        status = lost_workflow_status(project_name, workflow_name)
    redis.connection.set(p_status, json.dumps(status))


def add_task(project_name: str, workflow_name: str, task_name: str) -> None:
    """
    Adds a task to the workflow shared as PENDING. This function assumes it will
    be called in the task execution order.

    :param project_name: Project name.
    :param workflow_name: Workflow name.
    :param task_name: Task name.
    :return:
    """
    p_status = f"{project_name}_{workflow_name}_status"
    status = redis.connection.get(p_status)
    if status:
        status = json.loads(status)
        if not status_is_lost(status):
            status["tasks"][task_name] = new_task_status(task_name, "PENDING")
            status["order"].append(task_name)
    else:
        status = lost_workflow_status(project_name, workflow_name)
    redis.connection.set(p_status, json.dumps(status))


def update_task(project_name: str, workflow_name: str, task_name: str,
                state: str) -> None:
    """
    Adds a task to the workflow shared as PENDING. This function assumes all
    tasks have been previously registered through add_status.

    :param project_name: Project name.
    :param workflow_name: Workflow name.
    :param task_name: Task name.
    :param state: Task current state.
    :return:
    """
    p_status = f"{project_name}_{workflow_name}_status"
    status = redis.connection.get(p_status)
    if status:
        status = json.loads(status)
        if not status_is_lost(status):
            status["tasks"][task_name]["state"] = state
    else:
        status = lost_workflow_status(project_name, workflow_name)
    redis.connection.set(p_status, json.dumps(status))


def get_status(project_name: str, workflow_name: str) -> Optional[dict]:
    """
    Get the current shared of a workflow execution. Tasks will be returned as an
    ordered list instead of a dict.

    :param project_name: Project name.
    :param workflow_name: Workflow name.
    :return:
    """
    p_status = f"{project_name}_{workflow_name}_status"
    status = redis.connection.get(p_status)
    if status:
        status = json.loads(status)
        tasks = [
            status["tasks"][task_name] for task_name in status["order"]
        ]
        del status["order"]
        status["tasks"] = tasks
        return status
    else:
        return None
