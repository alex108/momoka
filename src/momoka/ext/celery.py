from flask import Flask
from celery import Celery as BaseCelery


class Celery(BaseCelery):
    flask: Flask

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if "app" in kwargs:
            self.app = kwargs["app"]
        else:
            self.app = None
        self.patch_task()

    def patch_task(self):
        _celery = self

        class ContextTask(self.Task):
            def __call__(self, *args, **kwargs):
                with _celery.flask.app_context():
                    return self.run(*args, **kwargs)

        setattr(self, "Task", ContextTask)

    def init_app(self, app: Flask):
        self.flask = app
        # Update include separately to always include the task modules
        # defined on creation.
        self.conf.include += app.config["CELERY"].get("include", [])
        self.conf.update({
            k: v for k, v in app.config["CELERY"].items()
            if k != "include"
        })
