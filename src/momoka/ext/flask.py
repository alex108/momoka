import os
import yaml

from flask import Flask as BaseFlask, Config as BaseConfig

__all__ = ["Flask"]


class Config(BaseConfig):
    """
    Flask config enhanced with a `from_yaml` method. Based on snippet
    https://gist.github.com/mattupstate/2046115.
    """

    def from_yaml(self, config_file):
        env = os.environ.get('FLASK_ENV', 'development')
        self['ENVIRONMENT'] = env.lower()

        with open(config_file) as f:
            c = yaml.load(f)

        c = c.get(env, c)

        for key in c:
            if key.isupper():
                self[key] = c[key]


class Flask(BaseFlask):
    """
    Extended version of `Flask` that implements custom config class. Based on
    snippet https://gist.github.com/mattupstate/2046115.
    """
    config: Config

    def make_config(self, instance_relative=False):
        root_path = self.root_path
        if instance_relative:
            root_path = self.instance_path
        return Config(root_path, self.default_config)


def make_flask(config_filename: str):
    """
    Flask application factory function. Based on
    http://flask.pocoo.org/docs/1.0/patterns/appfactories/.

    :param config_filename: Config file to load.
    :return:
    """
    app = Flask(__name__)
    app.config.from_yaml(config_filename)
