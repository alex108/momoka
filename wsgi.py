from momoka import make_flask

application = make_flask()

if __name__ == "__main__":
    application.run()
